# Parselog LDAP



## Getting started

Le programme parselog-ldap permet de construire des logs LDAP lisibles et exploitables. Chaque opération est sur une seule ligne avec l'IP source, l'utilisateur qui exécute la commande, la commande, les paramètres et le résultat (OK ou refused) ainsi qu'éventuellement le nombre de réponses envoyées. Il doit se lancer sur le serveur LDAP. Il ne peut pas recevoir les logs de plusieurs serveurs LDAP car il pourrait y avoir plusieurs fois le même numéro de connexion et donc des logs inconsistants.

Exemple de log obtenus:
```
ldaplis1 2023-10-31T06:00:01+01:00 ID=1698 SRC=172.28.68.115 BIND=Anonymous SRCH attr cn in "dc=lis-lab,dc=fr" with filter "(&(cn=service_info)(memberUid=XXX))" -> OK (1 answer)
ldaplis1 2023-10-31T06:00:01+01:00 ID=33104 SRC=127.0.0.1 BIND="cn=Manager,dc=lis-lab,dc=fr" BIND -> OK
ldaplis1 2023-10-31T06:00:01+01:00 ID=33104 SRC=127.0.0.1 BIND="cn=Manager,dc=lis-lab,dc=fr" DEL dn="uid=XXX,ou=People,dc=lis-lab,dc=fr" -> OK
ldaplis2 2023-10-31T06:00:01+01:00 ID=5457 SRC=139.124.22.7 BIND=Anonymous SRCH attr uid in "dc=lis-lab,dc=fr" with filter "(&(&(accountStatus=active)(businessCategory=XXX))(uid=XXX))" -> OK (0 answer)
```

Le programme prend en entrée un port sur lequel il va écouter en TCP sur 127.0.0.1 et va renvoyer les logs construits sur local5.info et les logs qui ne sont pas des opérations sur local5.debug. Il faudra passer en argument du programme le port d'écoute TCP:

```
parselog-ldap --listen=8080
```

Il faudra ensuite indiquer au rsyslog local d'envoyer les log LDAP sur le port d'écoute du démon et traiter les logs envoyés sur local5.info et local5.debug. Avec rsyslog, exemple d'un fichier /etc/rsyslog.d/ldap.conf:

```
local5.debug;local5.!info	-/var/log/ldap/ldap.log
local5.info	@@LOGHOST:PORT
local4.*        @@127.0.0.1:8080
:programname, isequal, "slapd" stop
```

Le démon parselog-ldap ne doit pas tourner en tant que root, il faut donc lui créer un utilisateur sous lequel il sera exécuté. 

## Compilation

Installer [rust](https://www.rust-lang.org/tools/install) puis: ```cargo build -r```

Le binaire sera présent dans target/release

## Fonctionnement

A la réception d'un log une première expression régulière va analyser la commande LDAP (BIND, ADD, DEL, SRCH...) et va envoyer le log dans la fonction adéquate. Les logs sont construits en récupérant les infos dans les logs et en utilisant le numéro de connexion et numéro d'opération. Tout est stocké dans des tables de hashage gérées dans le fichier hashmap.rs. Quand on tombe sur la commande RESULT, on va aller récupérer les infos préalablement stockées, construire la ligne de log à envoyer, l'envoyer avec la fonction sendlog() et nettoyer les tables de hashage.

## License
Aucune

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
