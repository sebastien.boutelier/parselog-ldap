use std::sync::Mutex;
use std::collections::HashMap;
use lazy_static::lazy_static;

lazy_static! {
    static ref HASHIP: Mutex<HashMap<String, String>> = {
        let m = HashMap::new();
        Mutex::new(m)
    };
    static ref HASHBIND: Mutex<HashMap<String, String>> = {
        let m = HashMap::new();
        Mutex::new(m)
    };
    static ref HASHCMD: Mutex<HashMap<String, String>> = {
        let m = HashMap::new();
        Mutex::new(m)
    };
    static ref HASHVAL1: Mutex<HashMap<String, String>> = {
        let m = HashMap::new();
        Mutex::new(m)
    };
    static ref HASHVAL2: Mutex<HashMap<String, String>> = {
        let m = HashMap::new();
        Mutex::new(m)
    };
    static ref HASHATTR: Mutex<HashMap<String, String>> = {
        let m = HashMap::new();
        Mutex::new(m)
    };
}

pub fn set_val(hash: &str, key: String,val: String){
    match hash {
      "ip"   => { let mut map = HASHIP.lock().unwrap(); map.insert(key,val); },
      "bind" => { let mut map = HASHBIND.lock().unwrap(); map.insert(key,val); },
      "cmd"  => { let mut map = HASHCMD.lock().unwrap(); map.insert(key,val); },
      "val1" => { let mut map = HASHVAL1.lock().unwrap(); map.insert(key,val); },
      "val2" => { let mut map = HASHVAL2.lock().unwrap(); map.insert(key,val); },
      "attr" => { let mut map = HASHATTR.lock().unwrap(); map.insert(key,val); },
       _      =>{},
    }
}

pub fn get_val(hash: &str, key: String)->String{
    match hash {
      "ip"   => { let map = HASHIP.lock().unwrap();   map.get(&key).unwrap_or(&"0".to_string()).to_string() },
      "bind" => { let map = HASHBIND.lock().unwrap(); map.get(&key).unwrap_or(&"Anonymous".to_string()).to_string() },
      "cmd"  => { let map = HASHCMD.lock().unwrap();  map.get(&key).unwrap_or(&"-".to_string()).to_string() },
      "val1" => { let map = HASHVAL1.lock().unwrap(); map.get(&key).unwrap_or(&"-".to_string()).to_string() },
      "val2" => { let map = HASHVAL2.lock().unwrap(); map.get(&key).unwrap_or(&"-".to_string()).to_string() },
      "attr" => { let map = HASHATTR.lock().unwrap(); map.get(&key).unwrap_or(&"-".to_string()).to_string() },
       _      =>{ "-".to_string()},
    }
}

pub fn removecon(key: String){
    let mut map = HASHIP.lock().unwrap();
    map.remove(&key);
    let mut map = HASHBIND.lock().unwrap();
    map.remove(&key);
}

pub fn removeop(key: String){
    let mut map = HASHCMD.lock().unwrap();
    map.remove(&key);
    let mut map = HASHVAL1.lock().unwrap();
    map.remove(&key);
    let mut map = HASHVAL2.lock().unwrap();
    map.remove(&key);
    let mut map = HASHATTR.lock().unwrap();
    map.remove(&key);
}
